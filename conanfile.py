from conans import CMake, ConanFile, tools


class XtensorConan(ConanFile):
    name = "xtensor"
    version = "0.20.5"
    license = "BSD3"
    url = "https://gitlab.com/mdavezac-conan/xtensor"
    homepage = "https://github.com/QuantStack/xtensor"
    description = "n-dimensional arrays with broadcasting and lazy computing."
    settings = "os", "arch", "compiler", "build_type"
    generators = "cmake"
    requires = ("xsimd/7.2.1@AstroFizz/stable", "xtl/0.6.4@AstroFizz/stable")
    no_copy_source = True

    def source(self):
        git = tools.Git(folder="xtensor")
        git.clone(self.homepage, branch=self.version)

    def configured_cmake(self):
        from os.path import join

        cmake = CMake(self)
        cmake.definitions["xtl_DIR"] = join(
            self.deps_cpp_info["xtl"].rootpath, "lib", "cmake", "xtl"
        )
        cmake.definitions["xsimd_DIR"] = join(
            self.deps_cpp_info["xsimd"].rootpath, "lib", "cmake", "xsimd"
        )
        cmake.configure(source_folder="xtensor")
        return cmake

    def build(self):
        return self.configured_cmake().build()

    def package(self):
        return self.configured_cmake().install()
